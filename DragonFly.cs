﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class DragonFly : Animal
    {
        public string Colour { get; set; }

        public override void Move()
        {
            Console.WriteLine("Fly");
        }
    }
}
