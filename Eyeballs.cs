﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Eyeballs
    {
        public Eyeball Left { get; set; }
        public Eyeball Right { get; set; }

        public void Look()
        {
            Console.WriteLine("I see");
        }
    }
}
