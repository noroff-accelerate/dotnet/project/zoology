﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    public interface IDig
    {
        public string DirtPreference { get; set; }
        public void Dig(string location);
        public void Fill();
    }
}
