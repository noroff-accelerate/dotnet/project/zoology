﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Grizzly grizzlyObj = new Grizzly()
            {
                Name = "Yogi",
                Height = 3.1,
                Species = "Alaskan",
                Diet = DietTypes.Omnivore,
                Weight = 280.5,
                Eyeballs = new Eyeballs()
                    {Left = new Eyeball() {Colour = "Brown"}, Right = new Eyeball() {Colour = "Brown"}}
            };

            Badger badgerObj = new Badger()
            {
                Height = 9, Name = "Lucky", Species = "Hunny",
                Weight = 76.9
            };

            DragonFly dfObj = new DragonFly() {Height = 0.8};

            grizzlyObj.Hunt(dfObj);
            grizzlyObj.Hunt(badgerObj);

            DigAHole(badgerObj);
        }

        public static void DigAHole(IDig digger)
        {
            digger.Dig("Right over here");
        }

        #region MethodExamples

        /// <summary>
        /// Greets the user and asks for their name. Then greets them by name
        /// </summary>
        public static void GreetAndPrompt()
        {
            Console.WriteLine("Hello World!, enter your name");
            string name = Console.ReadLine();
            Console.WriteLine($"Hello, {name} welcome to C#");
        }

        /// <summary>
        /// Adds two given numbers together and displays
        /// </summary>
        /// <param name="number1">First number</param>
        /// <param name="number2">Second number</param>
        public static int Add(int number1, int number2)
        {
            int sum = number1 + number2;
            return sum;
        }

        /// <summary>
        /// Gives bio information
        /// </summary>
        /// <returns>Bio Information</returns>
        public static string GetLecturerBio()
        {
            string bioInfo = "Super super good at all the stuff";

            return bioInfo;
        }

        /// <summary>
        /// Subtracts a number from a number
        /// </summary>
        /// <param name="number1">First number</param>
        /// <param name="number2">Second number</param>
        /// <returns></returns>
        public static int Subtract(int number1, int number2)
        {
            int result = number1 - number2;

            return result;
        }

        #endregion
    }
}
