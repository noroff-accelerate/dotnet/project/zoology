﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Grizzly : Animal
    {
        //Constructors
        public Grizzly()
        {
            Console.WriteLine("A Grizzly is born");
        }

        //Behaviour
        public override void Move()
        {
            Console.WriteLine("Run");
        }

        public void Hunt(Animal lunch)
        {
            if (lunch.Weight > 10)
            {
                Console.WriteLine("I totally ate that");
                Eat("Some creature big enough", 6);
            }
        }
    }
}
