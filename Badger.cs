﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Badger : Animal, IDig
    {
        public string DirtPreference { get; set; }

        public override void Move()
        {
            Console.WriteLine("Trod");
        }

        public void Dig(string location)
        {
            Console.WriteLine($"I totally dug a hole over there {location}");
        }

        public void Fill()
        {
            Console.WriteLine($"Fulfilled");
        }
    }
}
