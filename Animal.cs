﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    abstract class Animal
    {
        public string Name { get; set; }
        public double Weight { get; set; }
        public string Species { get; set; }
        public double Height { get; set; }

        public DietTypes Diet { get; set; }

        public Eyeballs Eyeballs { get; set; }

        public Brain Brain { get; set; }

        public Animal()
        {
            Brain = new Brain() {IQ = 10};
        }

        public void Eat(string food, double calories)
        {
            Console.WriteLine($"That {food} was tasty");
            Weight += calories;
        }

        public abstract void Move();
    }
}
