﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Eyeball
    {
        public string Colour { get; set; }
        public double RetinaDiameter { get; set; }
    }
}
